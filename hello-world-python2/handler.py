import time

def hello(event, context):
    print("this is the log info for playing with time and memory.")
    time.sleep(4)
    return "this is the function response for playing with time and memory."

